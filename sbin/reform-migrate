#!/bin/bash

set -e

echo ""
echo "This script can copy your current OS and files to a different disk, such as an SSD installed in MNT Reform."
echo ""
echo "Warning: This will overwrite files on the target partition. Make sure you know what you're doing!"
echo ""
echo "Before using this script, your target partition has to be formatted. You can use the gnome-disks program for that."
echo "gnome-disks can also help you set up an encrypted disk. You can also run the following commands for one single"
echo "partition on NVMe:"
echo ""
echo "    parted /dev/nvme0n1 \"mklabel msdos\""
echo "    parted /dev/nvme0n1 \"mkpart primary ext4 4MiB -1\""
echo "    mkfs.ext4 /dev/nvme0n1p1"
echo ""
echo "You can also setup an encrypted NVMe SSD by running reform-setup-encrypted-nvme"
echo ""

usage() {
	echo "Usage:"
	echo "  reform-migrate [--emmc] TARGET"
	echo ""
	echo "Mounts TARGET and copies the currently running system to it using rsync. This will"
	echo "overwrite all existing files in TARGET. At the end, reform-boot-config is run and"
	echo "will set up the first partition of the SD-card or eMMC (depending on the --emmc"
	echo "option) to load your newly copied rootfs and adjust /etc/fstab of TARGET accordingly."
	echo ""
	echo "Options:"
	echo ""
	echo "      --emmc Record boot preference on eMMC instead of SD card."
	echo "             This is only useful with SoM dip switch turned off."
	echo ""
	echo "Examples:"
	echo ""
	echo "  reform-migrate --emmc /dev/nvme0n1p1"
	echo "          Mounts the first partition of an NVMe disk and mirrors your system to it."
	echo "          Configures booting from the first partition on eMMC."
	echo "  reform-migrate /dev/mapper/crypt"
	echo "          Mounts encrypted disk 'crypt' and mirrors your system to it."
	echo "          The disk has to be unlocked first with: cryptsetup luksOpen /dev/nvme0n1p1 crypt"
	echo "          Configures booting from the first partition on the SD-card as --emmc option is missing."
}

if [ $# -ne 1 ] && [ $# -ne 2 ]; then
	usage
	exit 1
fi

EMMC=""
TARGET="$1"

if [ "--emmc" = "$1" ]; then
	EMMC="--emmc"
	TARGET="$2"
fi

if [ -z "$TARGET" ]; then
	usage
	exit 1
fi

if [ ! -e "$TARGET" ]
then
	echo "Error: The partition $TARGET does not exist."
	exit
fi

if [ -n "$(lsblk --noheadings --output=MOUNTPOINT "$TARGET")" ]; then
	echo "$TARGET is still in use" >&2
	exit 1
fi

if [ "$EUID" -ne 0 ]
  then echo "reform-migrate has to be run as root / using sudo."
  exit
fi

command -v "rsync" >/dev/null 2>&1 || { echo >&2 "Please install \"rsync\" using: apt install rsync"; exit 1; }

echo "Trying to mount $TARGET... (if this fails, format the disk/partition as explained above)"

MOUNTPOINT="$(mktemp --tmpdir --directory reform-migrate.XXXXXXXXXX)"

trap 'umount $MOUNTPOINT' EXIT INT TERM

mount "$TARGET" "$MOUNTPOINT"

echo "Target partition successfully mounted. The next step will copy all your files over to the target, overwriting existing files in the process."
echo ""

read -r -p "Are you sure you want to proceed? [y/N] " response

if [[ "$response" != "y" ]]
then
	echo "Exiting."
	umount "$MOUNTPOINT"
	trap - EXIT INT TERM
	exit
fi

echo "Starting the copy. This can take a while."

rsync -axHAWXS --numeric-ids --info=progress2 / "$MOUNTPOINT"

echo "Files copied."

umount "$MOUNTPOINT"
trap - EXIT INT TERM

echo "$TARGET unmounted."

echo "Running reform-boot-config..."

reform-boot-config $EMMC "$TARGET"
